/*
  Реалізувати функцію, яка отримуватиме масив елементів і 
  виводити їх на сторінку у вигляді списку. 
  Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:

- Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, 
  до якого буде прикріплений список (по дефолту має бути document.body.
кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

Приклади масивів, які можна виводити на екран:
["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
["1", "2", "3", "sea", "user", 23];

Можна взяти будь-який інший масив.

Необов'язкове завдання підвищеної складності:

- Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список.
Приклад такого масиву:
["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.

- Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.
*/

"use strict";
const createNestingList = (content) => {
  const nestingList = content
    .map((elemContent) => {
      const nestingElem = Array.isArray(elemContent)
        ? createNestingList(elemContent)
        : `<li>${elemContent}</li>`;
      return `${nestingElem}`;
    })
    .join(" ");
  return `<ul>${nestingList}</ul>`;
};

function insertListToPage(content, parentElement = document.body) {
  parentElement.insertAdjacentHTML("beforeend", createNestingList(content));
}

const timer = (parentElement, time) => {
  const showTime = (time) => {
    parentElement.innerHTML = `<b>Time left:</b> <i>${String(time).padStart(
      2,
      "0"
    )} s</i>`;
  };
  const clearTimer = setInterval(() => {
    if (time <= 0) {
      document.body.innerHTML = "";
      clearInterval(clearTimer);
    }
    parentElement.innerHTML = `<b>Time left:</b> <i>${String(
      --time + 1
    ).padStart(2, "0")} s</i>`;
  }, 1000);
  return parentElement;
};

// Tests
const testArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const testArray2 = ["1", "2", "3", "sea", "user", 23];
const nestingArray = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnipro",
  ["Novomoskovsk", "Pavlograd", ["Sloboghanske", "Doslidne"]],
];

const h1 = document.getElementsByTagName("h1")[0];
const timerContainer = document.createElement("div", { id: "timer" });
h1.before(timer(timerContainer, 3));
insertListToPage(testArray, document.getElementById("targetContainer"));
insertListToPage(testArray2);
insertListToPage(nestingArray);
